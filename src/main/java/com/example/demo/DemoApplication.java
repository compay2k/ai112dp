package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import ai112.meal.impl.factory.WinterMealFactory;
import ai112.meal.manager.MealManager;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		
		MealManager mm = new MealManager(new WinterMealFactory());

		mm.setFullMeal(true);

		return mm.toString();
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}