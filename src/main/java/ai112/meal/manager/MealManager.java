package ai112.meal.manager;

import ai112.meal.api.factory.MealFactory;
import ai112.meal.api.food.Dessert;
import ai112.meal.api.food.MainCourse;
import ai112.meal.api.food.Starter;

public class MealManager {
    
    private MealFactory mealFactory;

    private Starter starter;
    private MainCourse mainCourse;
    private Dessert dessert;

    public MealManager(MealFactory mealFactory){
        this.mealFactory = mealFactory;
    }

    public void setFullMeal(boolean isVegie)
    {
        this.starter = mealFactory.createStarter(isVegie);
        this.mainCourse = mealFactory.createMainCourse(isVegie);
        this.dessert = mealFactory.createDessert(isVegie);
    }

    public String toString(){
        return starter.toString() + " "
        + mainCourse.toString() + " "
        + dessert.toString();
    }

}
