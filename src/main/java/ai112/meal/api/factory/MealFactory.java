package ai112.meal.api.factory;

import ai112.meal.api.food.Dessert;
import ai112.meal.api.food.MainCourse;
import ai112.meal.api.food.Starter;

public abstract class MealFactory {
    
    public abstract Starter createStarter(boolean isVeggie);
    public abstract MainCourse createMainCourse(boolean isVeggie);
    public abstract Dessert createDessert(boolean isVeggie);
}
