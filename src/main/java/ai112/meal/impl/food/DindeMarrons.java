package ai112.meal.impl.food;

import ai112.meal.api.food.MainCourse;

public class DindeMarrons extends MainCourse{
    
    public DindeMarrons(){
        this.ingredients = new String[] {"dinde", "chataignes"};
    }

}
