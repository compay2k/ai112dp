package ai112.meal.impl.food;

import ai112.meal.api.food.Starter;

public class Escargots extends Starter {
    
    public Escargots(){
        this.ingredients = new String[] {"escargots", "beurre", "ail"};
    }

}
