package ai112.meal.impl.food;

import ai112.meal.api.food.Starter;

public class Tofu extends Starter {
    
    public Tofu(){
        this.ingredients = new String[] {"soja", "eau"};
    }

}
