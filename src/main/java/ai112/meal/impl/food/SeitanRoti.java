package ai112.meal.impl.food;

import ai112.meal.api.food.MainCourse;

public class SeitanRoti extends MainCourse{
    
    public SeitanRoti(){
        this.ingredients = new String[] {"seitan", "roti"};
    }
}
