package ai112.meal.impl.food;

import ai112.meal.api.food.Dessert;

public class BucheNoel extends Dessert{
    
    public BucheNoel(){
        this.ingredients = new String[] {"buche", "lutin"};
    }

}