package ai112.meal.impl.factory;

import ai112.meal.api.factory.MealFactory;
import ai112.meal.api.food.Dessert;
import ai112.meal.api.food.MainCourse;
import ai112.meal.api.food.Starter;
import ai112.meal.impl.food.BucheNoel;
import ai112.meal.impl.food.DindeMarrons;
import ai112.meal.impl.food.Escargots;
import ai112.meal.impl.food.SeitanRoti;
import ai112.meal.impl.food.Tofu;

public class WinterMealFactory extends MealFactory{

    @Override
    public Starter createStarter(boolean isVeggie) {
        if (isVeggie)
        {
            return new Tofu();
        }
        else
        {
            return new Escargots();
        }
    }

    @Override
    public MainCourse createMainCourse(boolean isVeggie) {
        if (isVeggie)
        {
            return new SeitanRoti();
        }
        else
        {
            return new DindeMarrons();
        }
    }

    @Override
    public Dessert createDessert(boolean isVeggie) {
        return new BucheNoel();
    }
    
}
