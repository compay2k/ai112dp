package ai112.state;

public class Commande {
    
    private EtatCommande etatCourant;

    public Commande()
    {
        etatCourant = new Creation();
    }

    public void Valider(){
        System.out.print("Valider - Etat avant : " + etatCourant.getClass().getSimpleName() + " ");
        etatCourant = etatCourant.valider(this);
        System.out.println(" ... nouvel état : " + etatCourant.getClass().getSimpleName());
    }

    public void annuler(){
        System.out.print("Annuler - Etat avant : " + etatCourant.getClass().getSimpleName() + " ");
        etatCourant = etatCourant.annuler(this);
        System.out.println("... nouvel état : " + etatCourant.getClass().getSimpleName());
    }


    protected void genererFacture()
    {
        System.out.print("Génération de la facture...");
    }

    protected void genererbonLivraison()
    {
        System.out.print("Génération du bon de livraison...");
    }

    protected void genererAccuseReception()
    {
        System.out.print("Génération de l'accusé réception...");
    }


}
