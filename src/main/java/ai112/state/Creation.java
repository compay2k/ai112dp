package ai112.state;

public class Creation extends EtatCommande{
    
    @Override
    public EtatCommande valider(Commande commande){
        commande.genererFacture();
        return new AttentePaiement();
    }

    @Override
    public EtatCommande annuler(Commande commande) {
        return new Annulee();
    }

}
