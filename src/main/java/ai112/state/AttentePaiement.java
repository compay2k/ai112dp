package ai112.state;

public class AttentePaiement extends EtatCommande{
    
    @Override
    public EtatCommande valider(Commande commande){
        commande.genererbonLivraison();
        return new EnLivraison();
    }

    @Override
    public EtatCommande annuler(Commande commande) {
        return new Annulee();
    }
}
