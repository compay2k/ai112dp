package ai112.state;

public abstract class EtatCommande {
    
    public EtatCommande valider(Commande commande) {
        return this;
    }

    public EtatCommande annuler(Commande commande) {
        return this;
    }

}
