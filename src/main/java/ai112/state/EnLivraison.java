package ai112.state;

public class EnLivraison extends EtatCommande{
    
    @Override
    public EtatCommande valider(Commande commande){
        commande.genererAccuseReception();
        return new Livree();
    }

}
